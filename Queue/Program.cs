﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        public static bool xZyZx (string str)
        {
            // תרגיל 21 בחומר ששלחת לי בוואטסאפ
            // counter_total counts the amount chars in the first 'x' and subtracting it in the sec 'x' in the scound 'else if'.
            Stack<char> middle = new Stack<char>();
            Queue<char> first = new Queue<char>();
            int counter_z = 0;
            int counter_total = 0;
            foreach(char ch in str)
            {
                if (ch != 'Z')
                {
                    if (counter_z == 0)
                    {
                        first.Enqueue(ch);
                        counter_total++;
                    }
                    else if (counter_z == 1)
                        middle.Push(ch);
                    else if (counter_z == 2)
                    {
                        if ((ch == first.Dequeue()) && (ch == middle.Pop()))
                            counter_total--;
                    }
                }
                else
                    counter_z++;
            }
            if (counter_total == 0)
                return true;
            else
                return false;
        }

        public static string opositestring (string str)
        {
            // ths function turn the place and the letter's order.
            // EX 20
            Stack<char> order = new Stack<char>();
            string strnew = "";
            string return_ = "";
            foreach (char ch in str)
                order.Push(ch);
            while (order.Count() > 0)
                strnew += order.Pop();
            string[] sen = strnew.Split(' ');
            for(int i = sen.Length -1; i >=0; i--)
                return_ +=" " + sen[i];
            return return_;
        }

        public static string Idonthaveaname(string str)
        {
            //EX 19
            /*הפעולה מקבלת מחרוזת שמחולקת באמצעות
             *@ 
             * כאשר כל 
             * @
             * הופך את סדר הקריאה של בתוכנית
             * למשל:
             * ne@m rev@ind
             * ידפיס
             * never mind
             */
            int counterstrudel = 0;
            string newstr = "";
            Stack<char> stack = new Stack<char>();
            foreach(char ch in str)
            {
                if (ch == '@')
                {
                    counterstrudel++;
                    if (counterstrudel % 2 == 0)
                        while (stack.Count() > 0)
                            newstr += stack.Pop();
                }
                else
                {
                    if (counterstrudel % 2 == 0)
                        newstr += ch;
                    else
                        stack.Push(ch);
                }
            }
            return newstr;
        }

        public static Queue<int> middle (Queue<int> que, int num)
        {
            // this action returns new Queue of the midlle 'x' number were asked.
            // the original QUEUE DOES NOT CHENGE
            Queue<int> newque = new Queue<int>();
            int mid = (que.Count() - num) / 2;
            for (int i=0; i< mid;i++)
                que.Enqueue(que.Dequeue());
            for (int i = 0; i < num; i++)
            {
                newque.Enqueue(que.Peek());
                que.Enqueue(que.Dequeue());
            }
                for (int i = 0; i <= mid; i++)
                que.Enqueue(que.Dequeue());
            return newque;
        }
        
        public static string theleader (Queue<string> names)
        {
            // this function gets a Queue of names and decides how will be the leader 
            // the original Queue does not chenge!
            Random rand = new Random();
            Queue<string> same = new Queue<string>();
            while(names.Count()>1)
            {
                    int randomnumber = rand.Next(2, 10);
                    for (int i = 0; i < randomnumber; i++)
                        names.Enqueue(names.Dequeue());
                    same.Enqueue(names.Dequeue());
            }
            int count = same.Count();
            for (int i = 0; i <count; i++)
                names.Enqueue(same.Dequeue());
            return names.Peek().ToString();
        }

        static void Main(string[] args)
        {
        }
    }
}
